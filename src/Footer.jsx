import React from "react";

export const Footer = () => {
  return (
    <div className="footie">
      <span>
        Table by <a href="https://coinsandscrolls.blogspot.com/">Skerples</a> |
        Tool by <a href="https://rpg-toolbox.blogspot.com/">Cassie</a>
      </span>
      <span>
        <a href="https://gitlab.com/cassie_/skerples_knights">
          This tool is open-source
        </a>
      </span>
    </div>
  );
};
