import React, { useState } from "react";
import { createLegendaryKnight } from "./legendary_knights";

const generateKnights = () => {
  let a = [];
  for (let i = 0; i < 5; i++) {
    a.push(createLegendaryKnight());
  }
  return a;
};

export const KnightsOfLegend = () => {
  const [knights, newKnights] = useState(generateKnights());

  return (
    <div className="QuestingKnights">
      <header className="App-header">
        <h2>Knights of Legend</h2>
      </header>
      <div className="body">
        <span className="button" onClick={() => newKnights(generateKnights)}>
          These are terrible!
        </span>
        {knights.map((knight, index) => {
          return (
            <div className="knight" key={index}>
              <p className="name">{knight.name}</p>
              <p className="name">{knight.famous_quest}</p>
              <p className="name">{knight.eccentricity}</p>
            </div>
          );
        })}
      </div>
    </div>
  );
};
