const titles = [
  "Sir",
  "Sir",
  "Sir",
  "Sir",
  "Sir",
  "Sir",
  "Sir",
  "Lady",
  "Lady",
  "Lady",
  "Lady",
  "Lady",
  "Lady",
  "Lady",
  "The Knight", "Baron", "Dread", "Defender", "Legate",
  "Protector", "Herald", "Warrior-Lord"
];

const maleNames = [
  "Kensingknot", "Alhoon", "Geoffrey", "Rexwell", "Oswald", "Guy", "Eckhart",
  "Koss", "Cantrell", "Huberis", "Hamilbil", "Roald"
];

const femaleNames = [
  "Alicia", "Scoborth", "Matilda", "Frigia", "Geromina", "Joan"
]

const neutralNames = [
  "Hedgewalloper", "Scoots"
];

const familyNames = [
  "of Barwell", "the Dour", "of Blution", "the Merciless", "of Treckett",
  "Truthspeaker", "of Lursingion", "the Pure", "the Ascetic", "Dragonslayer",
  "of Scarburl", "the Blue", "of Blood Royal", "the Foreigner",
  "of Necker's End", "the Skeptic", "of Rose Hill", "of Anderwells", "the Brave",
  "the Scourge"
];

const inconvenientQuests = [
  "Guards a passage, bridge, or gate",
  "Collects a legal but highly unfair tax on commerce",
  "Accompanies travellers (and bores them with tales",
  "Interrogates people, searching for ancestral crimes",
  "Attacks impous or rude travellers",
  "Enforces archaic weaponry and sumptuary laws",
  "Warns people away from a lucrative treasure site",
  "Flees from or destroys anything less than chaste",
  "Tries to convince people of the evil nature of wealth",
  "Cooks hearty but deeply suspicious meals for the poor",
  "Requires wizards to justify every spell and item",
  "Commands travellers to pay a visit to a nearby castle",
  "Kills anything that looks even remotely like a monster",
  "Insists people provide maps and full itineraries",
  "Accompanies travellers (and sing endlessly jolly songs)",
  "Examines all printed material for heretical ideas",
  "Interprets dreams, seeking hints of future calamities",
  "Howls unbelievable curses at sinners and heathens",
  "Wants to die nobly and pointlessly for a worthy cause",
  "Cursed to ask a riddle. Answer or die."
];

export const randomElement = array => array[Math.floor(Math.random() * array.length)];

const getKnightlyName = (title) => {
  switch (title) {
    case "Sir":
      return randomElement(maleNames.concat(neutralNames))
    case "Lady":
      return randomElement(femaleNames.concat(neutralNames))
    default:
      return randomElement(maleNames.concat(neutralNames).concat(femaleNames))
  }
}

export const createInconvenientKnight = () => {
  const title = randomElement(titles)
  const firstName = getKnightlyName(title)
  const familyName = randomElement(familyNames)
  const quest = randomElement(inconvenientQuests)

  return { title, firstName, familyName, quest }
}