import React, { useState } from "react";
import { createInconvenientKnight } from "./inconvenient_knights";

const generateKnights = () => {
  let a = [];
  for (let i = 0; i < 5; i++) {
    a.push(createInconvenientKnight());
  }
  return a;
};

export const QuestingKnights = () => {
  const [knights, newKnights] = useState(generateKnights());

  return (
    <div className="QuestingKnights">
      <header className="App-header">
        <h2>Questing Knights of the Realm</h2>
      </header>
      <div className="body">
        <span className="button" onClick={() => newKnights(generateKnights)}>
          These are terrible!
        </span>
        {knights.map((knight, index) => {
          return (
            <div className="knight" key={index}>
              <p className="name">
                {knight.title} {knight.firstName} {knight.familyName}
              </p>
              <p className="name">{knight.quest}</p>
            </div>
          );
        })}
      </div>
    </div>
  );
};
