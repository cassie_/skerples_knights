import React from 'react';
import './App.css';
import { Footer } from './Footer';
import { KnightsOfLegend } from './KnightsOfLegend';
import { QuestingKnights } from './QuestingKnights';

function App() {
  // const names = []
  // for (let i = 0; i < 10; i++) {
  //   names.push(randomElement(base) + randomElement(suffix))
  // }

  return (
    <div className="App">
      <header className="App-header">
        <h1>Knights</h1>
      </header>
      <QuestingKnights />
      <KnightsOfLegend />
      <Footer />
    </div>
  );
}

export default App;
