import { randomElement } from './inconvenient_knights';

const names = [
  "Sir Gilbert Meller", "Lady June Weaver", "Sir Malfang of Gurpt",
  "Lady Trentwell", "The Bronze Knight", "Sir Frank the Giantslayer",
  "Lady Sybil Grey", "Sir Walter of Boswell", "Lady Judith the Just",
  "The Knight of Long Lake", "Sir Giles Roquefort", "Lady Bell of Orby",
  "Sir Edwin the Bold", "Lady Turbellina", "The Briarworth Knight",
  "Sir Laurentian", "The Indigo Rider", "Sir Gershwin the Red",
  "The Lady of the Meadows", "The Obsidian Knight"
];

const famousQuests = [
  "Seeks the Holy Chalice of Immortality",
  "Charts a route to the far side of the world",
  "Must slay the Black Dragon Tyranoceptus",
  "Reclaim the Lost Land of the Urgonzoli",
  "Any non-knight in armour must die",
  "Only fights larger and larger creatures",
  "Seeks the True Icon of the Four Martyrs",
  "Prepares a feast that will summon an angel",
  "Duels everyone to gain more combat skills",
  "Drags people into lake, drowns them",
  "Atones for a vile, unspeakable sin",
  "Seeks the Five Stones of Svendelbart",
  "Vengeance on the beast that claimed an eye",
  "Guards the relics of the King of Sibylan",
  "Seeks a cure for a poisoned lord",
  "Compelled to burn, salt, and scour the earth",
  "Slays any beast that isn't arguably a horse",
  "Seeks the Precarious Tower of Wisdom",
  "Wreaks terrible revenge on all murderers",
  "Inscrutable acts of shocking violence"
]

const eccentricities = [
  "Old and forgetful", "Armoured in books", "Inconveniently honourable",
  "Swarms of squires", "Silent, slowly regenerates", "Remarkably small and stout",
  "Sings hymns", "Obsessed with appearance", "Bedecked with weapons",
  "Aquatic. Constantly dripping", "Morbid and prone to fits",
  "Accompanied by hunting dogs", "Rides a giant squirrel", "Wields an enormous hammer",
  "Maniacal fixation on goal", "Wields a flaming sword", "Terrible sense of direction",
  "Reanimated corpse in armour", "Stalks by night"
]


export const createLegendaryKnight = () => {
  const name = randomElement(names)
  const famous_quest = randomElement(famousQuests)
  const eccentricity = randomElement(eccentricities)

  return { name, famous_quest, eccentricity }
}